chrome.extension.onMessage.addListener(function(request, sender) {
    var url = "feed.html?" + encodeURIComponent(request.href);
    url = chrome.extension.getURL(url);
    chrome.tabs.create({ url: url, index: sender.tab.index });
});