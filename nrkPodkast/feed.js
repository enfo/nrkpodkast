// Grab the querystring, removing question mark at the front and splitting on
// the ampersand.
var queryString = location.search.substring(1).split("&");

// The feed URL is the first component and always present.
var feedUrl = decodeURIComponent(queryString[0]);
var req;
var doc;
// Handles parsing the feed data we got back from XMLHttpRequest.
function handleResponse() {
    doc = req.responseXML;
    var title = doc.getElementsByTagName('title')[0].textContent;
    var itemsTags = doc.getElementsByTagName('item');
    
    var imageTags = doc.getElementsByTagName('image');
    var imageTag;
    for(var i=0; i<imageTags.length; i++)
    {
        if(imageTags[i].tagName == 'image'){
            imageTag = imageTags[i];
            break;
        }
    }
    
    var imageEl = document.getElementById('image');
    if(imageTag != undefined)
    {
        for(var i=0; i<imageTag.childNodes.length; i++)
        {
            var child=imageTag.childNodes[i];
            if(child.tagName == 'url'){
                imageEl.src=child.textContent;
            }
        }
    }
    
    var titleEl = document.getElementById('title');
    titleEl.textContent = title;
    document.title = titleEl.textContent;
    var itemsEl = document.getElementById('items');
    for(var i=0; i<itemsTags.length; i++)
    {
        var item = itemsTags[i];
        var title = item.getElementsByTagName('title')[0].textContent
        var desc = item.getElementsByTagName('description')[0].textContent;
        var url = item.getElementsByTagName('enclosure')[0].attributes['url'].value;
        
        var duration;
        var attrs = item.childNodes;
        for(var attr_ix in attrs)
        {
            var attr = attrs[attr_ix];
            if(attr.tagName == "itunes:duration")
            {
                duration = attr.textContent;
            }
        }
        
        var itemDiv = document.createElement('div');
        var urlA = document.createElement('a');
        var descDiv = document.createElement('div');
        var durationDiv = document.createElement('div');
        
        itemDiv.className="item";
        urlA.textContent = title;
        urlA.href = url;
        urlA.className = "url";
        descDiv.textContent = desc;
        descDiv.className = "desc";
        durationDiv.textContent = "(" + duration + ")";
        durationDiv.className = "duration";
        
        itemDiv.appendChild(urlA);
        itemDiv.appendChild(durationDiv);
        itemDiv.appendChild(descDiv);
        itemsEl.appendChild(itemDiv);
    }
}

function main()
{
  req = new XMLHttpRequest();
  // Not everyone sets the mime type correctly, which causes handleResponse
  // to fail to XML parse the response text from the server. By forcing
  // it to text/xml we avoid this.
  req.onload = handleResponse;
  req.overrideMimeType('text/xml');
  req.open("GET", feedUrl, true);
  req.send(null);
}

document.addEventListener('DOMContentLoaded', function () {
    main();
});